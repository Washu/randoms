dofile "config.lua"

solution "quanta"
    configure_solution("..", "quanta")

    project_lib("core", {}, {
        "external/rapidjson"
    })

    project_console("app", {"core"}, {
    })

    project_test(
        "core_tests",
        {
            "core"
        }
    )