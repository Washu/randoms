rootDir = path.getabsolute("..")
buildDir = path.join(rootDir, "build")

if _ACTION == "clean" then
    os.rmdir("bin")
    os.rmdir("build")
    os.rmdir("obj")
end

dofile "api.lua"

function configure_solution(dir, name)
    language "C++"

    configurations {
        "release",
        "debug"
    }

    platforms {
        "x32",
        "x64"
    }

    local solutionDir = name or path.getrelative(rootDir, path.getabsolute(dir))
    local targetDir   = path.join(buildDir, "target")
    local projectDir = path.join(buildDir, "projects", solutionDir, _ACTION)

    if _ACTION == "gmake" then
        premake.gcc.cc  = "clang"
        premake.gcc.cxx = "clang++"
        premake.gcc.ar  = "ar"
    end

    location(projectDir)

    configuration "windows"
        objdir "../build/obj/windows"
        targetdir   "../build/bin/windows"
        links { "ole32" }
        defines { "QUANTA_PLATFORM_WINDOWS" }

    configuration "linux"
        buildoptions { "-std=c++14"}
        objdir "../build/obj/linux"
        targetdir   "../build/bin/linux"
        links       { "dl" }
        defines { "QUANTA_PLATFORM_LINUX" }

    configuration "bsd"
        buildoptions { "-std=c++14"}
        objdir "../build/obj/bsd"
        targetdir   "../build/bin/bsd"
        defines { "QUANTA_PLATFORM_BSD" }

    configuration "macosx"
        buildoptions { "-std=c++14", "-stdlib=libc++"}
        objdir "../build/obj/darwin"
        targetdir   "../build/bin/darwin"
        links       { "CoreServices.framework", "CoreFoundation.framework" }
        defines { "QUANTA_PLATFORM_MAC" }

    configuration { "macosx", "gmake" }
        buildoptions { "-mmacosx-version-min=10.9" }
        linkoptions  { "-mmacosx-version-min=10.9" }

    configuration "debug"
        defines {
            "QUANTA_BUILD_DEBUG",
            "_DEBUG"
        }
        flags {
            "Symbols"
        }

    configuration "release"
        defines {
            "QUANTA_BUILD_RELEASE",
            "NDEBUG"
        }
        flags {
            "OptimizeSpeed"
        }
end