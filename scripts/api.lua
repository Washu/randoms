function project_lib(name, deps, additional_includes)
    project (name)
        kind "StaticLib"
        uuid(os.uuid("lib-" .. name))
        build_project_parts(name, deps, additional_includes)
end

function project_console(name, deps, additional_includes)
    project (name)
        kind "ConsoleApp"
        uuid(os.uuid("console-" .. name))
        build_project_parts(name, deps, additional_includes)
end

function project_test(name, deps)
    project (name)
        kind "ConsoleApp"
        uuid(os.uuid("test-" .. name))
        build_project_parts(name, deps, { "external/catch" })
end

function build_project_parts(name, deps, additional_includes)
    targetname (name)

    local moduleDir = path.join(rootDir, name)
    includedirs {
        path.join(moduleDir, "include"),
        path.join(moduleDir, "src")
    }

    files {
        path.join(moduleDir, "include", "**"),
        path.join(moduleDir, "src", "**")
    }

    pchHeaderFile = os.matchfiles(path.join(moduleDir, "src", "*pch.h"))[1]
    pchSourceFile = os.matchfiles(path.join(moduleDir, "src", "*pch.cpp"))[1]

    if pchHeaderFile ~= nil then
        if _ACTION == "xcode4" then
            pchheader(pchHeaderFile)
        else
            pchheader(path.getname(pchHeaderFile))
        end

        pchsource(pchSourceFile)
    end

    vpaths {
        ["include/*"]     = { path.join(moduleDir, "include", "**") },
        ["src/*"]         = { path.join(moduleDir, "src", "**") }
    }

    for i, dep in ipairs(deps) do
        includedirs {
            path.join(rootDir, dep, "include")
        }

        links {
            dep
        }
    end

    for i, dep in ipairs(additional_includes) do
        includedirs {
            path.join(rootDir, dep, "include")
        }
    end
end