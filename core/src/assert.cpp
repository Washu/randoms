// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "core/assert.h"
#include <cstdlib>
#include <cstdio>
#include <cstdarg>

#include <os/platform_error.h>

namespace quanta {
void ErrorAssertion (char const * expressionStr, int line, char const * filename, char const * fmt, ...) {
    const size_t kMaxMessageSize = 512;
    char message[kMaxMessageSize + 1] = {};
    char fmtMessage[kMaxMessageSize + 1];

    sprintf_s(fmtMessage, kMaxMessageSize, "[FATAL] Assertion Failed: %s(%d): %s\n%s\n", filename, line, expressionStr, fmt);

    va_list args;
    va_start(args, fmt);
    vsnprintf_s(message, kMaxMessageSize - 1, fmtMessage, args);
    va_end(args);
    
    PlatformErrorAssert(message);
}
}
