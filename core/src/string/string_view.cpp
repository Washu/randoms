// Copyright 2017 Sean Kent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#define _SCL_SECURE_NO_WARNINGS

#include "core/core.h"
#include "core/string/string_view.h"
#include <locale>

namespace quanta {
constexpr StringView::size_type StringView::npos;

StringView::size_type StringView::Copy (pointer dest, size_type count, size_type pos) const {
    QUANTA_ASSERT(pos < Length(), "Cannot copy from an offset greater than the length of the string.");
    auto rcount = std::min(count, Length() - pos);
    QUANTA_ASSERT(pos + rcount < Length(), "Cannot copy more bytes than are in the string.")
    auto start = m_ptr + pos;
    auto end = m_ptr + pos + rcount;

    std::copy_n(start, rcount, dest);

    return end - start;
}

bool StringView::Contains (value_type c) const {
    return Contains(StringView(&c, 1));
}

bool StringView::Contains (StringView const & other) const {
    if (other.Length() > Length())
        return false;

    bool found = false;
    for (size_type i = 0; i <= Length() - other.Length(); ++i) {
        auto ptr = m_ptr + i;
        if(*ptr == other[0]) {
            found = true;
            for (size_type j = 1; j < other.Length(); ++j) {
                if(ptr[j] != other[j]) {
                    found = false;
                    break;
                }
            }
            if (found)
                break;
        }
    }

    return found;
}

bool StringView::ContainsI (value_type c) const {
    return ContainsI(StringView(&c, 1));
}

bool StringView::ContainsI (StringView const & other) const {
    if (other.Length() > Length())
        return false;

    bool found = false;
    for (size_type i = 0; i <= Length() - other.Length(); ++i) {
        auto ptr = m_ptr + i;
        if (toupper(*ptr) == toupper(other[0])) {
            found = true;
            for (size_type j = 1; j < other.Length(); ++j) {
                if (toupper(ptr[j]) != toupper(other[j])) {
                    found = false;
                    break;
                }
            }
            if (found)
                break;
        }
    }

    return found;
}

bool StringView::StartsWith (StringView const & other) const {
    if (other.Length() > Length())
        return false;

    for (size_type i = 0; i < other.Length(); ++i) {
        if (m_ptr[i] != other[i])
            return false;
    }

    return true;
}

bool StringView::StartsWithI (StringView const & other) const {
    if (other.Length() > Length())
        return false;

    for (size_type i = 0; i < other.Length(); ++i) {
        if (toupper(m_ptr[i]) != toupper(other[i]))
            return false;
    }

    return true;
}

bool StringView::EndsWith (StringView const & other) const {
    if (other.Length() > Length())
        return false;

    for (auto i = Length(), j = other.Length(); j > 0; --i, --j) {
        if (m_ptr[i-1] != other[j-1])
            return false;
    }

    return true;
}

bool StringView::EndsWithI (StringView const & other) const {
    if (other.Length() > Length())
        return false;

    for (auto i = Length(), j = other.Length(); j > 0; --i, --j) {
        if (toupper(m_ptr[i-1]) != toupper(other[j-1]))
            return false;
    }

    return true;
}

StringView::size_type StringView::FindFirstOf (value_type c, size_type offset) const {
    return FindFirstOf(StringView(&c, 1), offset);
}

StringView::size_type StringView::FindFirstOf (StringView const & tokens, size_type offset) const {
    if (offset > Length())
        return npos;

    for (auto i = offset; i < Length(); ++i) {
        if (tokens.Contains(m_ptr[i]))
            return i;
    }

    return npos;
}

StringView::size_type StringView::FindLastOf (value_type c, size_type offset) const {
    return FindLastOf(StringView(&c, 1), offset);
}

StringView::size_type StringView::FindLastOf (StringView const & tokens, size_type offset) const {
    if (offset == npos)
        offset = 0;

    if (offset > Length())
        return npos;

    for (auto i = Length(); i > offset; --i) {
        if (tokens.Contains(m_ptr[i - 1]))
            return i - 1;
    }

    return npos;
}
}
