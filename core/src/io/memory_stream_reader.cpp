// Copyright 2017 Sean Kent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "core/core.h"
#include "core/io/memory_stream_reader.h"
#include "core/io/dynamic_memory_stream_writer.h"

namespace quanta {
MemoryStreamReader::MemoryStreamReader (void const * ptr, uint64_t length) : m_ptr(static_cast<char const *>(ptr)), m_length(length) {
    QUANTA_ASSERT(m_ptr != nullptr, "Pointer cannot be null.");
}

MemoryStreamReader::MemoryStreamReader (DynamicMemoryStreamWriter const & writer) : m_ptr(static_cast<char const *>(writer.GetRawPointer())), m_length(writer.GetNumBytesWritten()) {
    QUANTA_ASSERT(m_ptr != nullptr, "Pointer cannot be null.");
}

uint64_t MemoryStreamReader::GetReadPosition () const {
    return m_offset;
}

Result MemoryStreamReader::SetReadPosition (uint64_t pos) {
    VALIDATE(pos < m_length, "Read position cannot be past the end.");
    m_offset = pos;

    return{};
}

Result MemoryStreamReader::ReadBytes (void * destBuffer, uint64_t numBytes) {
    QUANTA_ASSERT(m_offset + numBytes < std::numeric_limits<size_t>::max(), "Too many bytes.");

    VALIDATE(numBytes <= GetNumBytesRemaining(), "Cannot read more bytes than there are in the stream.");

    memcpy(destBuffer, m_ptr + m_offset, static_cast<size_t>(numBytes));
    m_offset += numBytes;
    return{};
}

uint64_t MemoryStreamReader::GetNumBytesRemaining () const {
    return m_length - m_offset;
}
}
