// Copyright 2017 Sean Kent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "core/core.h"
#include "core/io/dynamic_memory_stream_writer.h"

namespace quanta {
uint64_t DynamicMemoryStreamWriter::GetWritePosition () const {
    return m_writePos;
}

Result DynamicMemoryStreamWriter::SetWritePosition (uint64_t absolutePos) {
    m_writePos = absolutePos;
    return{};
}

Result DynamicMemoryStreamWriter::WriteBytes (void const * data, uint64_t countBytes) {
    QUANTA_ASSERT(m_writePos + countBytes < std::numeric_limits<size_t>::max(), "Too many bytes.");

    if ((m_writePos + countBytes) >= m_data.size()) {
        m_data.resize(static_cast<size_t>(m_writePos + countBytes));
    }

    memcpy(m_data.data() + m_writePos, data, static_cast<size_t>(countBytes));
    m_writePos += countBytes;

    return{};
}

uint64_t DynamicMemoryStreamWriter::GetNumBytesWritten () const {
    return m_data.size();
}

void * DynamicMemoryStreamWriter::GetRawPointer () const {
    return const_cast<DynamicMemoryStreamWriter *>(this)->m_data.data();
}
}
