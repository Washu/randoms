// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "core/io/json_scope_writer.h"

#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

namespace quanta {
struct JsonScopeWriterData {
    JsonScopeWriterData () {
        writer.Reset(buffer);
    }

    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer;
    rapidjson::StringBuffer buffer;
};

void JsonScopeWriterDataDeletor::operator() (JsonScopeWriterData * data) const {
    delete data;
}

JsonScopeWriter::JsonScopeWriter () : m_data(new JsonScopeWriterData()) {
}

JsonScopeWriter::~JsonScopeWriter () {
    JsonScopeWriter::Close();
}

Result JsonScopeWriter::Open (IStreamWriter & writer) {
    VALIDATE(m_writer == nullptr, "Open can only be called once.");

    m_writer = &writer;
    return {};
}

Result JsonScopeWriter::Close () {
    VALIDATE(m_scopeCount == 0, "Not all scopes were closed.");

    if (m_writer) {
        m_writer->WriteBytes(m_data->buffer.GetString(), m_data->buffer.GetLength());
    }

    m_writer = nullptr;
    return {};
}

Result JsonScopeWriter::BeginScope (StringHash const & name) {
    VALIDATE(m_data != nullptr, "Open must be called first.");

    if (name.GetHash() == 0) {
        VALIDATE(m_scopeCount == 0, "Only root objects can be nameless.");
    }
    else {
        VALIDATE(m_currentScopes.back().insert(name).second == true, "Scope already contains another entry with that name.");

        m_data->writer.Key(name.GetHashString());
    }

    m_currentScopes.push_back({});

    ++m_scopeCount;
    m_data->writer.StartObject();
    return {};
}

Result JsonScopeWriter::EndScope () {
    FAIL_RETURN(ValidateInternalState(0));

    m_currentScopes.pop_back();
    m_data->writer.EndObject();
    --m_scopeCount;

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, uint8_t const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Uint(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, uint16_t const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Uint(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, uint32_t const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Uint(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, uint64_t const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Uint64(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, int8_t const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Int(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, int16_t const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Int(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, int32_t const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Int(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, int64_t const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Int64(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, float const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Double(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, double const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.Double(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::WriteArray (StringHash const & name, std::string const * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.StartArray();
    for (auto i = 0u; i < count; ++i) {
        m_data->writer.String(value[i]);
    }
    m_data->writer.EndArray();

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, uint8_t value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Uint(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, uint16_t value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Uint(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, uint32_t value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Uint(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, uint64_t value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Uint64(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, int8_t value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Int(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, int16_t value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Int(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, int32_t value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Int(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, int64_t value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Int64(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, float value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Double(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, double value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Double(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, std::string const & value) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.String(value);

    return {};
}

Result JsonScopeWriter::Write (StringHash const & name, std::nullptr_t) {
    FAIL_RETURN(ValidateInternalState(name));

    m_data->writer.Key(name.GetHashString());
    m_data->writer.Null();

    return {};
}

Result JsonScopeWriter::ValidateInternalState (StringHash const & name) {
    VALIDATE(m_writer != nullptr, "Open must be called first.");
    VALIDATE(m_scopeCount > 0, "BeginScope must be called first.");
    if (name.GetHash() != 0) {
        VALIDATE(m_currentScopes.back().insert(name).second == true, "Scope already contains another entry with that name.");
    }

    return {};
}
}
