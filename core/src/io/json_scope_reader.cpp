// Copyright 2017 Sean Kent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "core/io/json_scope_reader.h"
#include "rapidjson/document.h"

namespace quanta {
struct JsonScopeReaderData {
    std::vector<char> data;
    rapidjson::Document document;
    std::vector<std::reference_wrapper<rapidjson::Value>> values;
};

void JsonScopeReaderDataDeletor::operator() (JsonScopeReaderData * data) const {
    delete data;
}

JsonScopeReader::~JsonScopeReader () {
}

Result JsonScopeReader::Open (IStreamReader & reader) {
    VALIDATE(m_reader == nullptr, "Open can only be called once.");

    m_data.reset(new JsonScopeReaderData());

    QUANTA_ASSERT(std::numeric_limits<size_t>::max() >= reader.GetNumBytesRemaining(), "Too many bytes.");

    m_data->data.resize(static_cast<size_t>(reader.GetNumBytesRemaining()) + 1);

    FAIL_RETURN(reader.ReadBytes(m_data->data.data(), reader.GetNumBytesRemaining()));

    m_data->document.ParseInsitu(m_data->data.data());
    if(m_data->document.HasParseError()) {
        m_data = nullptr;
        return Result("Failed to parse document.");
    }

    m_reader = &reader;

    return {};
}

Result JsonScopeReader::Close () {
    m_data = nullptr;
    m_reader = nullptr;

    return {};
}

Result JsonScopeReader::BeginScope (StringHash const & name) {
    if (name.GetHash() != 0) {
        auto hashStr = name.GetHashString();

        VALIDATE(m_data->values.back().get().HasMember(hashStr), "Failed to find scope matching name.");
        VALIDATE(m_data->values.back().get()[hashStr.c_str()].IsObject(), "Passed name was not a scope.");

        m_data->values.push_back(m_data->values.back().get()[hashStr]);
    }
    else {
        VALIDATE(m_data->values.size() == 0, "Cannot open the empty scope more than once.")
        m_data->values.push_back(m_data->document);
    }
    return {};
}

Result JsonScopeReader::EndScope () {
    FAIL_RETURN(ValidateInternalState(0));
    m_data->values.pop_back();
    return {};
}

Result JsonScopeReader::GetArrayLength (StringHash const & name, uint32_t & count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    count = prop.GetArray().Size();

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, uint8_t * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsUint(), "Expected unsigned integer.");
        auto v = array[i].GetUint();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, uint16_t * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsUint(), "Expected unsigned integer.");
        auto v = array[i].GetUint();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, uint32_t * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsUint(), "Expected unsigned integer.");
        auto v = array[i].GetUint();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, uint64_t * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsUint64(), "Expected unsigned integer.");
        auto v = array[i].GetUint64();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, int8_t * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsInt(), "Expected integer.");
        auto v = array[i].GetInt();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, int16_t * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsInt(), "Expected integer.");
        auto v = array[i].GetInt();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, int32_t * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsInt(), "Expected integer.");
        auto v = array[i].GetInt();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, int64_t * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsInt64(), "Expected integer.");
        auto v = array[i].GetInt64();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, float * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsDouble(), "Expected float.");
        auto v = array[i].GetDouble();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, double * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsDouble(), "Expected float.");
        auto v = array[i].GetDouble();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::ReadArray (StringHash const & name, std::string * value, size_t count) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsArray(), "Passed name was not an array.");
    auto const & array = prop.GetArray();
    VALIDATE(array.Size() == count, "Array sizes did not match.");

    for (auto i = 0u; i < count; ++i) {
        VALIDATE(array[i].IsString(), "Expected string.");
        auto v = array[i].GetString();
        value[i] = static_cast<std::remove_pointer_t<decltype(value)>>(v);
    }

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, uint8_t & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsUint(), "Expected unsigned integer.");
    auto v = prop.GetUint();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, uint16_t & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsUint(), "Expected unsigned integer.");
    auto v = prop.GetUint();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, uint32_t & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsUint(), "Expected unsigned integer.");
    auto v = prop.GetUint();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, uint64_t & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsUint64(), "Expected unsigned integer.");
    auto v = prop.GetUint64();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, int8_t & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsInt(), "Expected integer.");
    auto v = prop.GetInt();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, int16_t & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsInt(), "Expected integer.");
    auto v = prop.GetInt();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, int32_t & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsInt(), "Expected integer.");
    auto v = prop.GetInt();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, int64_t & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsInt64(), "Expected integer.");
    auto v = prop.GetInt64();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, float & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsDouble(), "Expected float.");
    auto v = prop.GetDouble();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, double & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsDouble(), "Expected float.");
    auto v = prop.GetDouble();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, std::string & value) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsString(), "Expected string.");
    auto v = prop.GetString();
    value = static_cast<std::remove_reference_t<decltype(value)>>(v);

    return {};
}

Result JsonScopeReader::Read (StringHash const & name, std::nullptr_t) {
    FAIL_RETURN(ValidateInternalState(name));

    auto const & prop = m_data->values.back().get()[name.GetHashString()];
    VALIDATE(prop.IsNull(), "Expected null.");

    return {};
}

Result JsonScopeReader::ValidateInternalState (StringHash const & name) const {
    VALIDATE(m_reader != nullptr, "Open must be called first.");
    VALIDATE(m_data->values.size() > 0, "BeginScope must be called first.");
    if (name.GetHash() != 0) {
        VALIDATE(m_data->values.back().get().HasMember(name.GetHashString()), "Expected field was not found.");
    }

    return {};
}
}
