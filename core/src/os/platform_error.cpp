// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <core/core.h>
#include <core/os/windows.h>
#include "platform_error.h"

#ifdef QUANTA_PLATFORM_WINDOWS
#pragma warning(push)
#pragma warning(disable : 4091)
#include <dbghelp.h>
#pragma comment(lib, "dbghelp.lib")
#pragma warning(pop)
#endif

namespace quanta {
#ifdef QUANTA_PLATFORM_WINDOWS
const DWORD kUserExceptionCode = 0xE0000001;

static bool s_initializedSeh = false;

static LONG WINAPI PlatformUnhandledExceptionHandler (EXCEPTION_POINTERS * info) {
    if (info->ExceptionRecord->ExceptionCode == kUserExceptionCode) {
        PlatformDebugBreak();
    }

    char moduleFileName[MAX_PATH];
    GetModuleFileNameA(nullptr, moduleFileName, MAX_PATH);
    strcat_s(moduleFileName, MAX_PATH, ".dmp");

    auto handle = CreateFileA(moduleFileName, GENERIC_READ | GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 0, nullptr);
    if (handle == INVALID_HANDLE_VALUE)
        return EXCEPTION_CONTINUE_SEARCH;

    MINIDUMP_EXCEPTION_INFORMATION mdei;
    mdei.ClientPointers = false;
    mdei.ExceptionPointers = info;
    mdei.ThreadId = GetCurrentThreadId();

    MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), handle, MiniDumpWithProcessThreadData, &mdei, nullptr, nullptr);
    CloseHandle(handle);

    return EXCEPTION_CONTINUE_SEARCH;
}

static BOOL CALLBACK EnumLoadedModulesCallback(PCSTR name, DWORD64 , ULONG , PVOID context) {
    bool * isManaged = reinterpret_cast<bool *>(context);
    StringView moduleName(name);

    auto pathEnd = moduleName.FindLastOf("\\/");
    if(pathEnd != StringView::npos) {
        moduleName = moduleName.SubStr(pathEnd + 1);
    }

    if(moduleName.StartsWithI("mscor")) {
        *isManaged = true;
        return FALSE;
    }
    
    return TRUE;
}

void PlatformInit () {
    if (s_initializedSeh)
        return;

    ::SetUnhandledExceptionFilter(&PlatformUnhandledExceptionHandler);

    bool isManaged = false;
    EnumerateLoadedModules64(GetCurrentProcess(), EnumLoadedModulesCallback, &isManaged);

    if (!isManaged && IsDebuggerPresent()) {
        AddVectoredExceptionHandler(0, &PlatformUnhandledExceptionHandler);
    }

    s_initializedSeh = true;
}

void PlatformErrorAssert (char const * msg) {
    if (!s_initializedSeh) {
        // We've hit a an assertion before platform init was called. This can happen during static initialization.
        PlatformInit();
    }

    fprintf(stderr, msg);
    OutputDebugStringA(msg);
    RaiseException(kUserExceptionCode, 0, 1, reinterpret_cast<const ULONG_PTR *>(&msg));
}

void PlatformDebugBreak () {
    if (IsDebuggerPresent())
        DebugBreak();
}

#else  //QUANTA_PLATFORM_WINDOWS

void PlatformErrorAssert (char const * msg) {
    abort();
}

void PlatformInit () {
}

void PlatformDebugBreak () {
}

#endif //QUANTA_PLATFORM_WINDOWS
}
