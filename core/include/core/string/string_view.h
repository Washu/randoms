// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "core/core.h"

namespace quanta {
class StringView {
public:
    using size_type = size_t;
    using value_type = char;
    using reference = value_type &;
    using const_reference = value_type const &;
    using pointer = value_type *;
    using const_pointer = value_type const *;
    using const_iterator = const_pointer;
    using iterator = const_iterator;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using reverse_iterator = const_reverse_iterator;
    using char_traits = std::char_traits<char>;

    static constexpr size_type npos = size_type(-1);

    struct CStrWrapper {
        constexpr CStrWrapper (const_pointer str) : str(str) {
        }

        const_pointer str;
    };

    struct StringWrapper {
        constexpr StringWrapper (std::string const & str) : str(str) {
        }

        std::string const & str;
    };

    constexpr StringView () : m_ptr(nullptr), m_len(0) {
    }

    constexpr StringView (StringView const & other) = default;

    template <size_t N>
    constexpr StringView (char const (&str)[N]) : m_ptr(str), m_len(N - 1) {
    }

    StringView (StringWrapper str) : m_ptr(str.str.data()), m_len(str.str.size()) {
    }

    StringView (CStrWrapper str) : m_ptr(str.str), m_len(char_traits::length(str.str)) {
    }

    constexpr StringView (const_pointer ptr, size_type len) : m_ptr(ptr), m_len(len) {
    }

    StringView & operator = (StringView const & other) = default;

    constexpr size_type Length () const {
        return m_len;
    }

    constexpr const_pointer Data () const {
        return m_ptr;
    }

    constexpr bool IsEmpty () const {
        return Length() == 0;
    }

    constexpr const_iterator begin () const {
        return m_ptr;
    }

    constexpr const_iterator cbegin () const {
        return begin();
    }

    constexpr const_iterator end () const {
        return m_ptr + m_len;
    }

    constexpr const_iterator cend () const {
        return end();
    }

    const_reverse_iterator rbegin () const {
        return const_reverse_iterator(end());
    }

    const_reverse_iterator rend () const {
        return const_reverse_iterator(begin());
    }

    const_reverse_iterator rcbegin () const {
        return rbegin();
    }

    const_reverse_iterator rcend () const {
        return rend();
    }

    constexpr const_reference operator[] (size_t index) const {
        return m_ptr[index];
    }

    void RemovePrefix (size_type n) {
        m_ptr += n;
        m_len -= n;
    }

    void RemoveSuffix (size_type n) {
        m_len -= n;
    }

    std::string ToString () const {
        return std::string(m_ptr, m_ptr + m_len);
    }

    explicit operator std::string () const {
        return ToString();
    }

    size_type Copy (pointer dest, size_type count, size_type pos = 0) const;

    bool Contains (value_type c) const;
    bool Contains (StringView const & other) const;

    bool ContainsI (value_type c) const;
    bool ContainsI (StringView const & other) const;

    bool StartsWith (StringView const & other) const;
    bool StartsWithI (StringView const & other) const;

    bool EndsWith (StringView const & other) const;
    bool EndsWithI (StringView const & other) const;

    size_type FindFirstOf (value_type c, size_type offset = 0) const;
    size_type FindFirstOf (StringView const & tokens, size_type offset = 0) const;

    size_type FindLastOf (value_type c, size_type offset = npos) const;
    size_type FindLastOf (StringView const & tokens, size_type offset = npos) const;

    constexpr StringView SubStr (size_type pos = 0, size_type count = npos) const {
        return StringView(m_ptr + pos, std::min(count, Length() - pos));
    }

    int Compare (StringView v) const {
        return char_traits::compare(m_ptr, v.m_ptr, std::min(m_len, v.m_len));
    }

    bool operator == (StringView v) const {
        return m_len == v.Length() && Compare(v) == 0;
    }

    bool operator != (StringView v) const {
        return !(*this == v);
    }
private:
    const_pointer m_ptr;
    size_type m_len;
};
}
