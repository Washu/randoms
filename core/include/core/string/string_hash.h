// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstdint>
#include <cstring>

#include "core/feature.h"

namespace quanta {
template <size_t N, size_t I>
struct ConstStrFnvHash {
    static constexpr uint32_t Hash (char const (&val)[N]) {
        return (ConstStrFnvHash<N, I - 1>::Hash(val) ^ val[I - 1]) * 16777619u;
    }
};

template <size_t N>
struct ConstStrFnvHash<N, 1> {
    static constexpr uint32_t Hash (char const (&val)[N]) {
        return (2166136261u ^ val[0]) * 16777619u;
    }
};

inline uint32_t StrFnvHash (char const * str, size_t len) {
    if (len == 0)
        return 0;

    auto hash = 2166136261u;

    for (size_t i = 0; i < len; ++i) {
        hash ^= *str++;
        hash *= 16777619u;
    }

    return hash;
}

class StringHash {
public:
    struct CStrWrapper {
        CStrWrapper (char const * str) : str(str) {
        }

        char const * str;
    };

    struct StringWrapper {
        StringWrapper (std::string const & str) : str(str) {
        }

        std::string const & str;
    };

    StringHash () : m_hash(0) {
    }

    StringHash (uint32_t hash) : m_hash(hash) {
    }

    StringHash (char const * str, size_t len) : m_hash(StrFnvHash(str, len)) {
#if QUANTA_FEATURE_ENABLE_DEV_STRINGS
        m_str = std::string(str, str + len);
#endif
    }

    StringHash (CStrWrapper wrapper) : m_hash(StrFnvHash(wrapper.str, strlen(wrapper.str))) {
#if QUANTA_FEATURE_ENABLE_DEV_STRINGS
        m_str = wrapper.str;
#endif
    }

    StringHash (StringWrapper wrapper) : m_hash(StrFnvHash(wrapper.str.c_str(), wrapper.str.size())) {
#if QUANTA_FEATURE_ENABLE_DEV_STRINGS
        m_str = wrapper.str;
#endif
    }

    StringHash (char const (&val)[1]) : m_hash(0) {
    }

    template <size_t N>
    StringHash (char const (&val)[N]) : m_hash(ConstStrFnvHash<N, N - 1>::Hash(val)) {
#if QUANTA_FEATURE_ENABLE_DEV_STRINGS
        m_str = val;
#endif
    }

    bool operator == (StringHash const & other) const {
        return m_hash == other.m_hash;
    }

    bool operator != (StringHash const & other) const {
        return m_hash != other.m_hash;
    }

    bool HasDevString () const {
#if QUANTA_FEATURE_ENABLE_DEV_STRINGS
        return !m_str.empty();
#else
        return false;
#endif
    }

    char const * GetDevString () const {
#if QUANTA_FEATURE_ENABLE_DEV_STRINGS
        return m_str.c_str();
#else
        return nullptr;
#endif
    }

    uint32_t GetHash () const {
        return m_hash;
    }

    std::string GetHashString () const {
#if QUANTA_FEATURE_ENABLE_DEV_STRINGS
        return m_str;
#else
        return std::to_string(m_hash);
#endif
    }

private:
    uint32_t m_hash;
#if QUANTA_FEATURE_ENABLE_DEV_STRINGS
    std::string m_str;
#endif
};
}

namespace std {
template <>
struct hash<quanta::StringHash> {
    size_t operator () (quanta::StringHash const & value) const {
        return value.GetHash();
    }
};
}
