// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <cstddef>

namespace quanta {
class ResultId {
public:
    static const ResultId Custom;

#ifdef QUANTA_BUILD_DEBUG
    template <size_t N>
    explicit ResultId (char const (&msg)[N]) : m_message(msg) {
    }

    char const * Message () const {
        return m_message;
    }

private:
    char const * m_message;
#else
    template <size_t N>
    explicit ResultId(char const (&msg)[N]) {
    }

    char const * Message() const {
        return "";
    }
#endif
};

class Result {
public:
#ifdef QUANTA_BUILD_DEBUG
    Result () : m_id(nullptr), m_message("") {
    }

    template <size_t N>
    explicit Result (char const (&msg)[N]) : m_id(&ResultId::Custom), m_message(msg) {
    }

    explicit Result (ResultId const & id) : m_id(&id), m_message(id.Message()) {
    }

    template <size_t N>
    Result (char const (&msg)[N], ResultId const & id) : m_id(&id), m_message(msg) {
    }

    char const * Message () const {
        return m_message;
    }
#else
    Result() : m_id(nullptr) {}

    template <size_t N>
    Result(char const (&msg)[N]) : m_id(&ResultId::Custom) {}

    Result(ResultId const & id) : m_id(&id) {}

    template <size_t N>
    Result(char const (&msg)[N], ResultId const & id) : m_id(&id) {}

    char const * Message() const { return ""; }
#endif

    explicit operator bool () const {
        return m_id == nullptr;
    }

    bool operator== (ResultId const & other) const {
        return m_id == &other;
    }

    bool operator!= (ResultId const & other) const {
        return m_id != &other;
    }

private:
    const ResultId * m_id;
#ifdef QUANTA_BUILD_DEBUG
    char const * m_message;
#endif
};

#define FAIL_RETURN(expr)      \
    {                          \
        auto result_ = (expr); \
        if (!result_)          \
            return result_;    \
    }

#define VALIDATE(expr, msg)               \
    {                                     \
        if (!(expr))                      \
            return ::quanta::Result(msg); \
    }
}
