// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "core/core.h"
#include "core/io/istream_reader.h"

namespace quanta {
struct IScopeReader {
    static const ResultId kResultNotFound;
    static const ResultId kResultNotNull;

    virtual ~IScopeReader () {
    }

    virtual Result Open (IStreamReader & reader) = 0;
    virtual Result Close () = 0;

    virtual Result BeginScope (StringHash const & name) = 0;
    virtual Result EndScope () = 0;

    virtual Result GetArrayLength (StringHash const & name, uint32_t & count) = 0;

    virtual Result ReadArray (StringHash const & name, uint8_t * value, size_t count) = 0;
    virtual Result ReadArray (StringHash const & name, uint16_t * value, size_t count) = 0;
    virtual Result ReadArray (StringHash const & name, uint32_t * value, size_t count) = 0;
    virtual Result ReadArray (StringHash const & name, uint64_t * value, size_t count) = 0;

    virtual Result ReadArray (StringHash const & name, int8_t * value, size_t count) = 0;
    virtual Result ReadArray (StringHash const & name, int16_t * value, size_t count) = 0;
    virtual Result ReadArray (StringHash const & name, int32_t * value, size_t count) = 0;
    virtual Result ReadArray (StringHash const & name, int64_t * value, size_t count) = 0;

    virtual Result ReadArray (StringHash const & name, float * value, size_t count) = 0;
    virtual Result ReadArray (StringHash const & name, double * value, size_t count) = 0;

    virtual Result ReadArray (StringHash const & name, std::string * value, size_t count) = 0;

    virtual Result Read (StringHash const & name, uint8_t & value) = 0;
    virtual Result Read (StringHash const & name, uint16_t & value) = 0;
    virtual Result Read (StringHash const & name, uint32_t & value) = 0;
    virtual Result Read (StringHash const & name, uint64_t & value) = 0;
    virtual Result Read (StringHash const & name, int8_t & value) = 0;
    virtual Result Read (StringHash const & name, int16_t & value) = 0;
    virtual Result Read (StringHash const & name, int32_t & value) = 0;
    virtual Result Read (StringHash const & name, int64_t & value) = 0;
    virtual Result Read (StringHash const & name, float & value) = 0;
    virtual Result Read (StringHash const & name, double & value) = 0;
    virtual Result Read (StringHash const & name, std::string & value) = 0;
    virtual Result Read (StringHash const & name, std::nullptr_t) = 0;
};
}
