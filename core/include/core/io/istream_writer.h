// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "core/core.h"

namespace quanta {
struct IStreamWriter {
    virtual ~IStreamWriter () {
    }

    virtual uint64_t GetWritePosition () const = 0;
    virtual Result SetWritePosition (uint64_t absolutePos) = 0;
    virtual Result WriteBytes (void const * data, uint64_t countBytes) = 0;
    virtual uint64_t GetNumBytesWritten () const = 0;
};
}
