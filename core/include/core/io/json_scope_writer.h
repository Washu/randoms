// Copyright 2017 Sean Kent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "core/io/iscope_writer.h"

namespace quanta {
struct JsonScopeWriterData;

struct JsonScopeWriterDataDeletor {
    void operator () (JsonScopeWriterData * data) const;
};

class JsonScopeWriter final : public IScopeWriter {
public:
    JsonScopeWriter ();
    ~JsonScopeWriter ();

    virtual Result Open (IStreamWriter & writer) override;
    virtual Result Close () override;

    virtual Result BeginScope (StringHash const & name) override;
    virtual Result EndScope () override;

    virtual Result WriteArray (StringHash const & name, uint8_t const * value, size_t count) override;
    virtual Result WriteArray (StringHash const & name, uint16_t const * value, size_t count) override;
    virtual Result WriteArray (StringHash const & name, uint32_t const * value, size_t count) override;
    virtual Result WriteArray (StringHash const & name, uint64_t const * value, size_t count) override;

    virtual Result WriteArray (StringHash const & name, int8_t const * value, size_t count) override;
    virtual Result WriteArray (StringHash const & name, int16_t const * value, size_t count) override;
    virtual Result WriteArray (StringHash const & name, int32_t const * value, size_t count) override;
    virtual Result WriteArray (StringHash const & name, int64_t const * value, size_t count) override;

    virtual Result WriteArray (StringHash const & name, float const * value, size_t count) override;
    virtual Result WriteArray (StringHash const & name, double const * value, size_t count) override;

    virtual Result WriteArray (StringHash const & name, std::string const * value, size_t count) override;

    virtual Result Write (StringHash const & name, uint8_t value) override;
    virtual Result Write (StringHash const & name, uint16_t value) override;
    virtual Result Write (StringHash const & name, uint32_t value) override;
    virtual Result Write (StringHash const & name, uint64_t value) override;
    virtual Result Write (StringHash const & name, int8_t value) override;
    virtual Result Write (StringHash const & name, int16_t value) override;
    virtual Result Write (StringHash const & name, int32_t value) override;
    virtual Result Write (StringHash const & name, int64_t value) override;
    virtual Result Write (StringHash const & name, float value) override;
    virtual Result Write (StringHash const & name, double value) override;
    virtual Result Write (StringHash const & name, std::string const & value) override;
    virtual Result Write (StringHash const & name, std::nullptr_t) override;

private:
    Result ValidateInternalState (StringHash const & name);

    std::unique_ptr<JsonScopeWriterData, JsonScopeWriterDataDeletor> m_data;
    std::vector<std::unordered_set<StringHash>> m_currentScopes;

    IStreamWriter * m_writer = nullptr;
    int m_scopeCount = 0;
};
}
