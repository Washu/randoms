// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "core/core.h"
#include "core/io/istream_writer.h"

namespace quanta {
class DynamicMemoryStreamWriter : public IStreamWriter {
public:
    uint64_t GetWritePosition () const override;
    Result SetWritePosition (uint64_t absolutePos) override;
    Result WriteBytes (void const * data, uint64_t countBytes) override;
    uint64_t GetNumBytesWritten () const override;

    void * GetRawPointer () const;

private:
    uint64_t m_writePos = 0;
    std::vector<char> m_data;
};
}
