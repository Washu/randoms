// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "core/core.h"
#include "core/io/istream_reader.h"

namespace quanta {
class DynamicMemoryStreamWriter;

class MemoryStreamReader : public IStreamReader {
public:
    MemoryStreamReader (void const * ptr, uint64_t length);

    explicit MemoryStreamReader (DynamicMemoryStreamWriter const & writer);

    template <class T>
    MemoryStreamReader (std::vector<T> const & v) : MemoryStreamReader(v.data(), v.size() * sizeof(T)) {
    }

    uint64_t GetReadPosition () const override;
    Result SetReadPosition (uint64_t pos) override;
    Result ReadBytes (void * destBuffer, uint64_t numBytes) override;
    uint64_t GetNumBytesRemaining () const override;

private:
    char const * m_ptr;
    uint64_t m_length;
    uint64_t m_offset = 0;
};
}
