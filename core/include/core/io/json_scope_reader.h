// Copyright 2017 Sean Kent
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include "core/io/iscope_reader.h"

namespace quanta {
struct JsonScopeReaderData;

struct JsonScopeReaderDataDeletor {
    void operator () (JsonScopeReaderData *) const;
};

class JsonScopeReader final : public IScopeReader {
public:
    ~JsonScopeReader ();

    virtual Result Open (IStreamReader & reader) override;
    virtual Result Close () override;

    virtual Result BeginScope (StringHash const & name) override;
    virtual Result EndScope () override;

    virtual Result GetArrayLength (StringHash const & name, uint32_t & count) override;

    virtual Result ReadArray (StringHash const & name, uint8_t * value, size_t count) override;
    virtual Result ReadArray (StringHash const & name, uint16_t * value, size_t count) override;
    virtual Result ReadArray (StringHash const & name, uint32_t * value, size_t count) override;
    virtual Result ReadArray (StringHash const & name, uint64_t * value, size_t count) override;

    virtual Result ReadArray (StringHash const & name, int8_t * value, size_t count) override;
    virtual Result ReadArray (StringHash const & name, int16_t * value, size_t count) override;
    virtual Result ReadArray (StringHash const & name, int32_t * value, size_t count) override;
    virtual Result ReadArray (StringHash const & name, int64_t * value, size_t count) override;

    virtual Result ReadArray (StringHash const & name, float * value, size_t count) override;
    virtual Result ReadArray (StringHash const & name, double * value, size_t count) override;

    virtual Result ReadArray (StringHash const & name, std::string * value, size_t count) override;

    virtual Result Read (StringHash const & name, uint8_t & value) override;
    virtual Result Read (StringHash const & name, uint16_t & value) override;
    virtual Result Read (StringHash const & name, uint32_t & value) override;
    virtual Result Read (StringHash const & name, uint64_t & value) override;
    virtual Result Read (StringHash const & name, int8_t & value) override;
    virtual Result Read (StringHash const & name, int16_t & value) override;
    virtual Result Read (StringHash const & name, int32_t & value) override;
    virtual Result Read (StringHash const & name, int64_t & value) override;
    virtual Result Read (StringHash const & name, float & value) override;
    virtual Result Read (StringHash const & name, double & value) override;
    virtual Result Read (StringHash const & name, std::string & value) override;
    virtual Result Read (StringHash const & name, std::nullptr_t) override;

private:
    Result ValidateInternalState (StringHash const & name) const;

    std::unique_ptr<JsonScopeReaderData, JsonScopeReaderDataDeletor> m_data;
    IStreamReader * m_reader = nullptr;
};
}
