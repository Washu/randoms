// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "core/core.h"

#include <catch/catch.hpp>

TEST_CASE("StringHashTest-TestZeroAndEmptyStringAreEqual") {
    quanta::StringHash emptyHash;
    quanta::StringHash emptyConstantStr("");
    quanta::StringHash emptyStr(std::string(""));
    const char * str = "";
    quanta::StringHash emptyCStr(str);
    quanta::StringHash nonEmptyButLen0("test", 0);

    REQUIRE(0 == emptyHash.GetHash());
    REQUIRE(emptyHash == emptyConstantStr);
    REQUIRE(emptyHash == emptyStr);
    REQUIRE(emptyHash == emptyCStr);
    REQUIRE(emptyHash == nonEmptyButLen0);
}

TEST_CASE("StringHashTest-TestStrAreEqual") {
    quanta::StringHash constStr("test");
    quanta::StringHash cppStr(std::string("test"));
    const char * str = "test";
    quanta::StringHash cStr(str);
    quanta::StringHash lenStr("testxyzw", 4);

    REQUIRE(constStr == cppStr);
    REQUIRE(constStr == cStr);
    REQUIRE(constStr == lenStr);
}
