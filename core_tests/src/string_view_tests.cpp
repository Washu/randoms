// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <catch/catch.hpp>

#include "core/core.h"

TEST_CASE("StringView-BasicTests") {
    quanta::StringView emptyView;
    REQUIRE(emptyView.IsEmpty());

    quanta::StringView constStrView("hello");

    REQUIRE_FALSE(constStrView.IsEmpty());

    char const * cstr = "hello";
    std::string helloStr = "hello";
    quanta::StringView cStrView(cstr);
    quanta::StringView strView(helloStr);
    quanta::StringView cStrLenView("hello", 5);

    REQUIRE(cStrView[0] == cstr[0]);
    REQUIRE(cStrView[4] == helloStr[4]);

    REQUIRE(constStrView == cStrView);
    REQUIRE(constStrView == strView);
    REQUIRE(constStrView == cStrLenView);

    auto view = constStrView.SubStr(1);
    REQUIRE(view == "ello");

    REQUIRE(view != constStrView);

    constStrView.RemovePrefix(1);
    REQUIRE(constStrView == "ello");

    constStrView.RemoveSuffix(1);
    REQUIRE(constStrView == "ell");

    view = constStrView.SubStr(1, 1);
    REQUIRE(view == "l");

    constStrView = cStrView;

    quanta::StringView _1("1");
    quanta::StringView _2("2");

    REQUIRE(_1.Compare(_2) < 0);
    REQUIRE(_2.Compare(_1) > 0);

    REQUIRE(_1 != _2);

    char part[5] = {};
    auto len = cStrView.Copy(part, 3, 1);
    REQUIRE(len == 3);
    REQUIRE(quanta::StringView(part, 3) == "ell");
}

TEST_CASE("StringView-StartsWith") {
    quanta::StringView view("I start with I");

    REQUIRE(view.StartsWith("I start"));
    REQUIRE(view.StartsWithI("i START"));
    REQUIRE_FALSE(view.StartsWith("I not start"));
    REQUIRE_FALSE(view.StartsWithI("i NOT START"));
    REQUIRE_FALSE(view.StartsWith("I start with I "));
    REQUIRE_FALSE(view.StartsWithI("i start with i "));
}

TEST_CASE("StringView-EndsWith") {
    quanta::StringView view("I end with abcd");

    REQUIRE(view.EndsWith("abcd"));
    REQUIRE(view.EndsWithI("ABCD"));
    REQUIRE_FALSE(view.EndsWith(" "));
    REQUIRE_FALSE(view.EndsWith("eabcd"));
    REQUIRE_FALSE(view.EndsWith(" I end with abcd"));
    REQUIRE_FALSE(view.EndsWithI(" i end with abcd"));
}

TEST_CASE("StringView-Contains") {
    quanta::StringView view("I contain xyz");

    REQUIRE(view.Contains("ain"));
    REQUIRE_FALSE(view.Contains("AIN"));

    REQUIRE(view.Contains("I "));
    REQUIRE(view.Contains("xyz"));

    REQUIRE_FALSE(view.Contains("I contain xyz "));
    REQUIRE_FALSE(view.Contains('p'));

    REQUIRE(view.Contains('I'));
    REQUIRE(view.Contains(' '));
    REQUIRE(view.Contains('z'));

    REQUIRE(view.ContainsI("aIn"));

    REQUIRE(view.ContainsI("i "));
    REQUIRE(view.ContainsI("xYz"));

    REQUIRE_FALSE(view.ContainsI("i contain xyz "));
    REQUIRE_FALSE(view.ContainsI('P'));

    REQUIRE(view.ContainsI('i'));
    REQUIRE(view.ContainsI(' '));
    REQUIRE(view.ContainsI('Z'));
}

TEST_CASE("StringView-FindFirstOf") {
    quanta::StringView view("I contain xyzt");

    REQUIRE(0 == view.FindFirstOf("I"));
    REQUIRE(1 == view.FindFirstOf(" "));
    REQUIRE(view.Length() - 2 == view.FindFirstOf("z"));
    REQUIRE(view.FindFirstOf("not") == 3);
    REQUIRE(view.FindFirstOf("p") == quanta::StringView::npos);
    REQUIRE(view.FindFirstOf(" ", 1) == 1);
    REQUIRE(view.FindFirstOf(" ", 100) == quanta::StringView::npos);

    REQUIRE(view.FindFirstOf('t') == 5);
}


TEST_CASE("StringView-FindLastOf") {
    quanta::StringView view("I contain xyzt");

    REQUIRE(0 == view.FindLastOf("I"));
    REQUIRE(9 == view.FindLastOf(" "));
    REQUIRE(view.FindLastOf("I", 1) == quanta::StringView::npos);
    REQUIRE(view.FindLastOf("A") == quanta::StringView::npos);
    REQUIRE(view.FindLastOf("aiy") == 11);
    REQUIRE(view.FindLastOf(" ", 100) == quanta::StringView::npos);
    REQUIRE(view.FindLastOf("c", 2) == 2);
}
