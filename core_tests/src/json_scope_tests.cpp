// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "core/core.h"

#include "core/io/memory_stream_reader.h"
#include "core/io/dynamic_memory_stream_writer.h"

#include "core/io/json_scope_writer.h"
#include "core/io/json_scope_reader.h"

#include <catch/catch.hpp>

TEST_CASE("Json-BasicWriterTests") {
    quanta::DynamicMemoryStreamWriter memoryStream;

    quanta::JsonScopeWriter writer;

    REQUIRE(writer.Open(memoryStream));
    REQUIRE(writer.BeginScope(""));

    // Empty scope name is only valid for the root scope.
    REQUIRE_FALSE(writer.BeginScope(""));

    REQUIRE(writer.Write("int", 1));

    // We should not be able to write the same key twice to the same scope.
    REQUIRE_FALSE(writer.Write("int", 1));

    // But we should be able to write one to a subscope.
    REQUIRE(writer.BeginScope("subint"));
    REQUIRE(writer.Write("int", 1));
    REQUIRE(writer.EndScope());

    // We should not be able to begin a scope with the same name as another key.
    REQUIRE_FALSE(writer.BeginScope("int"));

    REQUIRE(writer.Write("uint", 2u));
    REQUIRE(writer.Write("int64", 3ll));
    REQUIRE(writer.Write("uint64", 4ull));

    REQUIRE(writer.Write("float", 1.0f));
    REQUIRE(writer.Write("double", 1.0));
    REQUIRE(writer.Write("string", "string"));

    REQUIRE(writer.Write("nullptr", nullptr));

    std::vector<int32_t> ints;
    std::vector<uint32_t> uints;
    std::vector<std::string> strings;
    for (auto i = 0; i < 10; ++i) {
        ints.push_back(i);
        uints.push_back(i);
        strings.push_back(std::to_string(i));
    }

    REQUIRE(writer.BeginScope("arrays"));
    REQUIRE(writer.WriteArray("ints", ints.data(), ints.size()));
    REQUIRE(writer.WriteArray("uints", uints.data(), uints.size()));
    REQUIRE(writer.WriteArray("strings", strings.data(), strings.size()));
    REQUIRE(writer.EndScope());

    // Cannot close the writer until all the scopes are closed.
    REQUIRE_FALSE(writer.Close());

    REQUIRE(writer.EndScope());

    // Cannot end a scope that does not exist
    REQUIRE_FALSE(writer.EndScope());

    REQUIRE(writer.Close());
}

TEST_CASE("Json-BasicReadTest") {
    std::string jsonText = R"({
        "int": 1,
        "uint": 2,
        "int64": 3,
        "uint64": 4,
        "float": 1.0,
        "double": 1.0,
        "string": "string",
        "nullptr": null,
        "arrays": {
            "ints": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            "uints": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            "strings": ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        }
    })";

    quanta::MemoryStreamReader memoryReader(jsonText.data(), jsonText.size());

    quanta::JsonScopeReader reader;
    REQUIRE(reader.Open(memoryReader));
    REQUIRE(reader.BeginScope(""));

    // Empty scope name is only valid at the root scope.
    REQUIRE_FALSE(reader.BeginScope(""));

    int32_t val;
    uint32_t uval;
    int64_t val64;
    uint64_t uval64;

    REQUIRE(reader.Read("int", val));
    REQUIRE(reader.Read("uint", uval));
    REQUIRE(reader.Read("int64", val64));
    REQUIRE(reader.Read("uint64", uval64));

    REQUIRE(1 == val);
    REQUIRE(2 == uval);
    REQUIRE(3 == val64);
    REQUIRE(4 == uval64);

    float f;
    double d;
    std::string s;

    REQUIRE(reader.Read("float", f));
    REQUIRE(reader.Read("double", d));

    REQUIRE(reader.Read("string", s));
    REQUIRE(reader.Read("nullptr", nullptr));

    REQUIRE(1.0f == f);
    REQUIRE(1.0 == d);
    REQUIRE("string" == s);

    REQUIRE(reader.BeginScope("arrays"));

    //
    {
        uint32_t count;
        REQUIRE(reader.GetArrayLength("ints", count));

        std::vector<int32_t> ints;
        ints.resize(count);
        REQUIRE(reader.ReadArray("ints", ints.data(), ints.size()));

        for (auto i = 0; i < 10; ++i) {
            REQUIRE(i == ints[i]);
        }
    }

    //
    {
        uint32_t count;
        REQUIRE(reader.GetArrayLength("uints", count));

        std::vector<uint32_t> uints;
        uints.resize(count);
        REQUIRE(reader.ReadArray("uints", uints.data(), uints.size()));

        for (auto i = 0; i < 10; ++i) {
            REQUIRE(i == uints[i]);
        }
    }

    //
    {
        uint32_t count;
        REQUIRE(reader.GetArrayLength("strings", count));

        std::vector<std::string> strings;
        strings.resize(count);
        REQUIRE(reader.ReadArray("strings", strings.data(), strings.size()));

        for (auto i = 0; i < 10; ++i) {
            REQUIRE(std::to_string(i) == strings[i]);
        }
    }

    REQUIRE(reader.EndScope());
    REQUIRE(reader.EndScope());

    // Cannot end a scope that does not exist.
    REQUIRE_FALSE(reader.EndScope());

    REQUIRE(reader.Close());
}

TEST_CASE("Json-WriteThenReadTest") {
    quanta::DynamicMemoryStreamWriter memoryStream;

    // Write out a bunch of json.
    {
        quanta::JsonScopeWriter writer;

        REQUIRE(writer.Open(memoryStream));

        REQUIRE(writer.BeginScope(""));
        REQUIRE_FALSE(writer.BeginScope(""));

        REQUIRE(writer.Write("int", 1));
        REQUIRE(writer.Write("uint", 2u));
        REQUIRE(writer.Write("int64", 3ll));
        REQUIRE(writer.Write("uint64", 4ull));

        REQUIRE(writer.Write("float", 1.0f));
        REQUIRE(writer.Write("double", 1.0));

        REQUIRE(writer.Write("string", "string"));
        REQUIRE(writer.Write("nullptr", nullptr));

        std::vector<int32_t> ints;
        std::vector<uint32_t> uints;
        std::vector<std::string> strings;
        for (auto i = 0; i < 10; ++i) {
            ints.push_back(i);
            uints.push_back(i);
            strings.push_back(std::to_string(i));
        }

        REQUIRE(writer.BeginScope("arrays"));
        REQUIRE(writer.WriteArray("ints", ints.data(), ints.size()));
        REQUIRE(writer.WriteArray("uints", uints.data(), uints.size()));
        REQUIRE(writer.WriteArray("strings", strings.data(), strings.size()));
        REQUIRE(writer.EndScope());

        REQUIRE(writer.EndScope());
        REQUIRE_FALSE(writer.EndScope());

        REQUIRE(writer.Close());
    }

    // and read it back in
    {
        quanta::MemoryStreamReader memoryReader(memoryStream.GetRawPointer(), memoryStream.GetNumBytesWritten());

        quanta::JsonScopeReader reader;
        REQUIRE(reader.Open(memoryReader));
        REQUIRE(reader.BeginScope(""));

        int32_t val;
        uint32_t uval;
        int64_t val64;
        uint64_t uval64;

        REQUIRE(reader.Read("int", val));
        REQUIRE(reader.Read("uint", uval));
        REQUIRE(reader.Read("int64", val64));
        REQUIRE(reader.Read("uint64", uval64));

        REQUIRE(1 == val);
        REQUIRE(2 == uval);
        REQUIRE(3 == val64);
        REQUIRE(4 == uval64);

        float f;
        double d;
        std::string s;

        REQUIRE(reader.Read("float", f));
        REQUIRE(reader.Read("double", d));

        REQUIRE(reader.Read("string", s));
        REQUIRE(reader.Read("nullptr", nullptr));

        REQUIRE(1.0f == f);
        REQUIRE(1.0 == d);
        REQUIRE("string" == s);

        REQUIRE(reader.BeginScope("arrays"));

        //
        {
            uint32_t count;
            REQUIRE(reader.GetArrayLength("ints", count));

            std::vector<int32_t> ints;
            ints.resize(count);
            REQUIRE(reader.ReadArray("ints", ints.data(), ints.size()));

            for (auto i = 0; i < 10; ++i) {
                REQUIRE(i == ints[i]);
            }
        }

        //
        {
            uint32_t count;
            REQUIRE(reader.GetArrayLength("uints", count));

            std::vector<uint32_t> uints;
            uints.resize(count);
            REQUIRE(reader.ReadArray("uints", uints.data(), uints.size()));

            for (auto i = 0; i < 10; ++i) {
                REQUIRE(i == uints[i]);
            }
        }

        //
        {
            uint32_t count;
            REQUIRE(reader.GetArrayLength("strings", count));

            std::vector<std::string> strings;
            strings.resize(count);
            REQUIRE(reader.ReadArray("strings", strings.data(), strings.size()));

            for (auto i = 0; i < 10; ++i) {
                REQUIRE(std::to_string(i) == strings[i]);
            }
        }

        REQUIRE(reader.EndScope());
        REQUIRE(reader.EndScope());
        REQUIRE_FALSE(reader.EndScope());
        REQUIRE(reader.Close());
    }
}
