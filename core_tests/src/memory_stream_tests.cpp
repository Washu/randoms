// Copyright 2017 Sean Kent
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "core/core.h"

#include "core/io/memory_stream_reader.h"
#include "core/io/dynamic_memory_stream_writer.h"

#include <catch/catch.hpp>

TEST_CASE("MemoryStreamReader-BasicReadTest") {
    std::vector<int> data;
    for (auto i = 0; i < 10; ++i)
        data.push_back(i);

    quanta::MemoryStreamReader memoryStream(data);

    REQUIRE(0 == memoryStream.GetReadPosition());
    REQUIRE(10 * sizeof(int) == memoryStream.GetNumBytesRemaining());

    int val;
    for (auto i = 0; i < 10; ++i) {
        REQUIRE(memoryStream.ReadBytes(&val, sizeof(int)));
        REQUIRE(val == i);
    }

    REQUIRE_FALSE(memoryStream.ReadBytes(&val, 1));

    REQUIRE(0 == memoryStream.GetNumBytesRemaining());
    REQUIRE(10 * sizeof(int) == memoryStream.GetReadPosition());

    REQUIRE_FALSE(memoryStream.SetReadPosition(41));
    REQUIRE(memoryStream.SetReadPosition(0));

    REQUIRE(0 == memoryStream.GetReadPosition());
    REQUIRE(10 * sizeof(int) == memoryStream.GetNumBytesRemaining());
}

TEST_CASE("DynamicMemoryStreamWriter-BasicWriteTest") {
    quanta::DynamicMemoryStreamWriter writer;

    REQUIRE(0 == writer.GetNumBytesWritten());
    REQUIRE(0 == writer.GetWritePosition());

    for (auto i = 0; i < 10; ++i) {
        REQUIRE(writer.WriteBytes(&i, sizeof(i)));
        REQUIRE(sizeof(int) * (i + 1) == writer.GetNumBytesWritten());
    }

    REQUIRE(10 * sizeof(int) == writer.GetNumBytesWritten());
    REQUIRE(10 * sizeof(int) == writer.GetWritePosition());

    REQUIRE(writer.SetWritePosition(0));

    REQUIRE(10 * sizeof(int) == writer.GetNumBytesWritten());
    REQUIRE(0 == writer.GetWritePosition());

    auto ptr = static_cast<int *>(writer.GetRawPointer());
    for (auto i = 0; i < 10; ++i) {
        REQUIRE(i == ptr[i]);
    }

    REQUIRE(writer.SetWritePosition(10 * sizeof(int) + sizeof(int)));
    int val = 11;
    REQUIRE(writer.WriteBytes(&val, sizeof(val)));
    REQUIRE(12 * sizeof(int) == writer.GetNumBytesWritten());

    ptr = static_cast<int *>(writer.GetRawPointer());
    REQUIRE(0 == ptr[10]);
    REQUIRE(11 == ptr[11]);

    REQUIRE(writer.SetWritePosition(10 * sizeof(int)));
    val = 10;
    writer.WriteBytes(&val, sizeof(int));
    REQUIRE(12 * sizeof(int) == writer.GetNumBytesWritten());

    ptr = static_cast<int *>(writer.GetRawPointer());
    REQUIRE(10 == ptr[10]);
    REQUIRE(11 == ptr[11]);
}

TEST_CASE("MemoryStreams-BasicReadWriteTest") {
    quanta::DynamicMemoryStreamWriter writer;

    REQUIRE(0 == writer.GetNumBytesWritten());
    REQUIRE(0 == writer.GetWritePosition());

    for (auto i = 0; i < 10; ++i) {
        REQUIRE(writer.WriteBytes(&i, sizeof(i)));
        REQUIRE(sizeof(int) * (i + 1) == writer.GetNumBytesWritten());
    }

    REQUIRE(10 * sizeof(int) == writer.GetNumBytesWritten());
    REQUIRE(10 * sizeof(int) == writer.GetWritePosition());

    quanta::MemoryStreamReader memoryStream(writer);

    REQUIRE(0 == memoryStream.GetReadPosition());
    REQUIRE(10 * sizeof(int) == memoryStream.GetNumBytesRemaining());

    int val;
    for (auto i = 0; i < 10; ++i) {
        REQUIRE(memoryStream.ReadBytes(&val, sizeof(int)));
        REQUIRE(val == i);
    }

    REQUIRE(0 == memoryStream.GetNumBytesRemaining());
    REQUIRE(10 * sizeof(int) == memoryStream.GetReadPosition());
}